package de.buchholz.gameoflife.test;

import junit.framework.Assert;
import junit.framework.TestCase;
import de.buchholz.gameoflife.game.GOLCell;
import de.buchholz.gameoflife.game.GameOfLife;

public class TestGOF extends TestCase {

	public void testNeighbor() {
		GameOfLife gol = new GameOfLife(80, 40);
		gol.resetBoard();

		gol.getBoard()[0][0] = new GOLCell(true);
		gol.getBoard()[1][0] = new GOLCell(true);
		gol.getBoard()[2][0] = new GOLCell(true);
		gol.getBoard()[2][1] = new GOLCell(true);
		gol.getBoard()[2][2] = new GOLCell(true);
		gol.getBoard()[1][2] = new GOLCell(true);
		gol.getBoard()[0][2] = new GOLCell(true);
		gol.getBoard()[0][1] = new GOLCell(true);
		gol.getBoard()[1][1] = new GOLCell(true);

		Assert.assertEquals(8, gol.countAliveNeighbors(1, 1));

		Assert.assertEquals(3, gol.countAliveNeighbors(0, 0));
	}

	public void testNeighbor2() {
		GameOfLife gol = new GameOfLife(80, 40);
		gol.resetBoard();

		gol.getBoard()[0][0] = new GOLCell(true);
		gol.getBoard()[1][1] = new GOLCell(true);
		gol.getBoard()[1][0] = new GOLCell(true);
		gol.getBoard()[0][1] = new GOLCell(true);

		Assert.assertEquals(3, gol.countAliveNeighbors(1, 1));

	}
}
