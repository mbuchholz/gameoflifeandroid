package de.buchholz.gameoflife.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import de.buchholz.gameoflife.game.GOLCell;
import de.buchholz.gameoflife.game.GameOfLife;
import de.buchholz.gameoflife.util.ColorCalc;

/**
 * View where the actual Game of Life is drawn on.
 * 
 * @author Michael
 * 
 */
public class GOLSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

	/** The thread that actually draws the animation */
	private GOLSurfaceThread mThread;

	/** Handle to the application context, used to e.g. fetch Drawables. */
	private Context mContext;

	final public String LOG_TAG = "GOLSurfaceView";

	/**
	 * Drawing Thread and Game of Life heartbeat thread. Draws directly on
	 * Canvas of {@link GOLSurfaceView}.
	 * 
	 * @author Michael
	 * 
	 */
	public class GOLSurfaceThread extends Thread {

		/**
		 * Game of Life Board
		 */
		private GameOfLife mGol;

		private Paint mLinePaint;
		private Paint mCellPaint;
		private Paint mBackgroundPaint;
		// Color.argb(0, 0, 51, 255);
		private int mCellPaintYoung = Color.argb(255, 102, 102, 255); // blue
		private int mCellPaintOld = Color.argb(255, 255, 0, 51); // red

		private int mCellAgeMax = 10;

		/**
		 * Current height of the surface/canvas.
		 * 
		 * @see #setSurfaceSize
		 */
		private int mCanvasHeight = 1;

		/**
		 * Current width of the surface/canvas.
		 * 
		 * @see #setSurfaceSize
		 */
		private int mCanvasWidth = 1;

		/** number of pixels for a cell */
		private int mCellSize = 20;

		/*
		 * State-tracking constants
		 */
		public static final int STATE_PAUSE = 1;
		public static final int STATE_READY = 2;
		public static final int STATE_RUNNING = 3;

		/** The state of the game. One of PAUSE, READY, RUNNING */
		private int mMode;

		/** Indicate whether the surface has been created & is ready to draw */
		private boolean mRun = false;

		/** Used to figure out elapsed time between frames */
		private long mLastTime;

		/** the "tick" of the clock in MS */
		private int mTick = 1000;

		/** Handle to the surface manager object we interact with */
		private SurfaceHolder mSurfaceHolder;

		private static final String KEY_BOARD_X = "mBoardX";
		private static final String KEY_BOARD_Y = "mBoardY";
		private static final String KEY_BOARD_LINE = "mBoardLine";

		public GOLSurfaceThread(SurfaceHolder surfaceHolder, Context context) {
			mSurfaceHolder = surfaceHolder;
			mContext = context;

			mLinePaint = new Paint();
			mLinePaint.setColor(0xff2c3037);
			mLinePaint.setAntiAlias(true);
			mLinePaint.setStrokeWidth(1);

			mCellPaint = new Paint();
			mCellPaint.setColor(0xff677199);
			mCellPaint.setAntiAlias(true);
			mCellPaint.setStrokeWidth(1);

			mBackgroundPaint = new Paint();
			mBackgroundPaint.setColor(0x88121416);
			mBackgroundPaint.setAntiAlias(true);
			mBackgroundPaint.setStrokeWidth(2);

		}

		/**
		 * Used to signal the thread whether it should be running or not.
		 * Passing true allows the thread to run; passing false will shut it
		 * down if it's already running. Calling start() after this was most
		 * recently called with false will result in an immediate shutdown.
		 * 
		 * @param b
		 *            true to run, false to shut down
		 */
		public void setRunning(boolean b) {
			mRun = b;
		}

		/**
		 * Sets the tick of the heartbeat.
		 * 
		 * @param tick
		 */
		public void setTick(int tick) {
			Log.i(LOG_TAG, "Setting heartbeat tick to:" + tick);
			this.mTick = tick;
		}

		/**
		 * Sets the mode. That is, whether we are running, paused...
		 * 
		 * @param mode
		 *            one of the STATE_* constants
		 */
		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				Log.i(LOG_TAG, "Setting state to: " + mode);
				mMode = mode;
			}
		}

		/* Callback invoked when the surface dimensions change. */
		public void setSurfaceSize(int width, int height) {
			// synchronized to make sure these all change atomically
			synchronized (mSurfaceHolder) {
				if (mCanvasWidth != width || mCanvasHeight != height) {
					mCanvasWidth = width;
					mCanvasHeight = height;

					mGol = new GameOfLife(width / mCellSize, height / mCellSize);
					mGol.generateRandomBoard();
				}
			}
		}

		@Override
		public void run() {
			while (mRun) {
				Canvas c = null;
				try {
					// don't draw anything if we're not in focus
					if (hasWindowFocus()) {
						c = mSurfaceHolder.lockCanvas(null);
						synchronized (mSurfaceHolder) {
							if (mMode == STATE_RUNNING) {
								updateGeneration();
							}
							doDraw(c);
						}
					}
				} finally {
					// do this in a finally so that if an exception is thrown
					// during the above, we don't leave the Surface in an
					// inconsistent state
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		private void updateGeneration() {
			long now = System.currentTimeMillis();

			// Do nothing if mLastTime is in the future.
			// This allows the game-start to delay the start of the physics
			// by 100ms or whatever.
			if (mLastTime > now) {
				return;
			}

			double elapsed = now - mLastTime;
			if (elapsed > mTick) {
				mGol.computeNextGenerationBoard();
				// update GOL
				mLastTime = now;
			}
		}

		/**
		 * Draws the ship, fuel/speed bars, and background to the provided
		 * Canvas.
		 */
		private void doDraw(Canvas canvas) {
			if (canvas != null) {
				// reset canvas
				canvas.drawColor(mBackgroundPaint.getColor(), Mode.CLEAR);
				canvas.drawRect(0, 0, mCanvasWidth, mCanvasHeight, mBackgroundPaint);

				// draw GRID
				for (int x = 0; x < mCanvasWidth; x = x + mCellSize) {
					canvas.drawLine(x, 0, x, mCanvasHeight, mLinePaint);
				}
				for (int y = 0; y < mCanvasHeight; y = y + mCellSize) {
					canvas.drawLine(0, y, mCanvasWidth, y, mLinePaint);
				}

				// draw CELLS
				GOLCell[][] board = mGol.getBoard();
				for (int x = 0; x < board.length; x++) {
					for (int y = 0; y < board[x].length; y++) {
						if (board[x][y].isAlive()) {
							int xc = x * mCellSize;
							int yc = y * mCellSize;
							float interpolateFactor = 0.0f;
							interpolateFactor = (float) board[x][y].getAge() / mCellAgeMax;
							if (interpolateFactor > 1.0f) {
								interpolateFactor = 1.0f;
							}
							mCellPaint.setColor(ColorCalc.getInterpolatedColor(mCellPaintYoung, mCellPaintOld,
									interpolateFactor));
							RectF rect = new RectF(xc, yc, xc + mCellSize, yc + mCellSize);
							canvas.drawRoundRect(rect, 3, 3, mCellPaint);
						}
					}
				}
			}
		}

		/**
		 * Pauses the update & animation.
		 */
		public void pause() {
			synchronized (mSurfaceHolder) {
				Log.i(LOG_TAG, "pause()");
				if (mMode == STATE_RUNNING) {
					setState(STATE_PAUSE);
				}
			}
		}

		public void resetBoard() {
			synchronized (mSurfaceHolder) {
				mGol.generateRandomBoard();
			}
		}

		/**
		 * Resumes from a pause.
		 */
		public void unpause() {
			Log.i(LOG_TAG, "unpause()");
			setState(STATE_RUNNING);

			// if the thread wasn't started yet do now
			if (mThread.getState() == State.NEW) {
				setRunning(true);
				mThread.start();
			}
		}

		public GameOfLife getGameOfLife() {
			return mGol;
		}

		public int getMode() {
			return mMode;
		}

		/**
		 * Dump game state to the provided Bundle. Typically called when the
		 * Activity is being suspended.
		 * 
		 * @return Bundle with this view's state
		 */
		public Bundle saveState(Bundle map) {
			Log.i(LOG_TAG, "saveState(Bundle)");
			synchronized (mThread.mSurfaceHolder) {
				if (map != null) {
					int boardDimensionX = mGol.getBoard().length;
					int boardDimensionY = mGol.getBoard()[0].length;
					map.putInt(GOLSurfaceThread.KEY_BOARD_X, boardDimensionX);
					map.putInt(GOLSurfaceThread.KEY_BOARD_Y, boardDimensionY);
					// for (int x = 0; x < boardDimensionX; x++) {
					// map.putBooleanArray(GOLSurfaceThread.KEY_BOARD_LINE + "_"
					// + x, mGol.getBoard()[x]);
					// }
				}
			}
			return map;
		}

		public synchronized void restoreState(Bundle savedState) {
			Log.i(LOG_TAG, "restoreState(Bundle)");
			synchronized (mThread.mSurfaceHolder) {
				int boardDimensionX = savedState.getInt(GOLSurfaceThread.KEY_BOARD_X);
				int boardDimensionY = savedState.getInt(GOLSurfaceThread.KEY_BOARD_Y);
				mGol = new GameOfLife(boardDimensionX, boardDimensionY);
				mGol.resetBoard();
				// for (int x = 0; x < boardDimensionX; x++) {
				// mGol.getBoard()[x] =
				// savedState.getBooleanArray(GOLSurfaceThread.KEY_BOARD_LINE +
				// "_" + x);
				// }
			}
		}

	}

	/**
	 * Ctor
	 * 
	 * @param context
	 * @param attrs
	 */
	public GOLSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);

		mContext = context;

		// register our interest in hearing about changes to our surface
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		// create&start the thread here so that we don't busy-wait in run()
		// waiting for the surface to be created
		mThread = new GOLSurfaceThread(holder, context);

		setFocusable(false); // make sure we get key events
	}

	/**
	 * Fetches the animation thread corresponding to this LunarView.
	 * 
	 * @return the animation thread
	 */
	public GOLSurfaceThread getThread() {
		return mThread;
	}

	/**
	 * Standard window-focus override. Notice focus lost so we can pause on
	 * focus lost. e.g. user switches to take a call.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		Log.i(LOG_TAG, "onWindowFocusChanged(" + hasWindowFocus + ")");
		if (!hasWindowFocus) {
			mThread.pause();
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		Log.i(LOG_TAG, "surfaceChanged(...)");
		mThread.setSurfaceSize(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		Log.i(LOG_TAG, "surfaceCreated(...)");
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.i(LOG_TAG, "surfaceDestroyed(...)");
	}

}
