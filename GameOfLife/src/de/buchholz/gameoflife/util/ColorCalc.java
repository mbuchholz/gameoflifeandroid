package de.buchholz.gameoflife.util;

import android.graphics.Color;

/**
 * Class that calculates the Color between two given Colors.
 * 
 * @author Michael
 * 
 */
public class ColorCalc {

	/**
	 * Return the Color between colorA and colorB while proportion defines the
	 * mix between the two colors.
	 * 
	 * @param colorA
	 *            Color A value
	 * @param colorB
	 *            Color B value
	 * @param proportion
	 *            Proportion between Color A and B. 0.0f means ColorA, 1.0f
	 *            means ColorB.
	 * @return
	 */
	public static int getInterpolatedColor(int colorA, int colorB, float proportion) {
		int a = ave(Color.alpha(colorA), Color.alpha(colorB), proportion);
		int r = ave(Color.red(colorA), Color.red(colorB), proportion);
		int g = ave(Color.green(colorA), Color.green(colorB), proportion);
		int b = ave(Color.blue(colorA), Color.blue(colorB), proportion);
		return Color.argb(a, r, g, b);
	}

	private static int ave(int src, int dst, float p) {
		return src + java.lang.Math.round(p * (dst - src));
	}
}
