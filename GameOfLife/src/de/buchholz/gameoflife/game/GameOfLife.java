package de.buchholz.gameoflife.game;

import java.io.IOException;

/**
 * Simple Game of Life implementation.<br/>
 * See http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life for rules.
 * 
 * @author Michael
 * 
 */
public class GameOfLife {
	// board dimension
	private final int boardX;
	private final int boardY;
	private static int generationCount = 0;

	private GOLCell[][] board;

	public GameOfLife(int x, int y) {
		this.boardX = x;
		this.boardY = y;
		this.board = resetBoard();
	}

	public GOLCell[][] resetBoard() {
		GOLCell[][] newBoard = new GOLCell[boardX][boardY];

		for (int y = 0; y < boardY; y++) {
			for (int x = 0; x < boardX; x++) {
				newBoard[x][y] = new GOLCell();
			}
		}

		return newBoard;
	}

	public static void main(String... args) throws IOException, InterruptedException {
		boolean doBreak = false;
		GameOfLife gol = new GameOfLife(80, 40);
		gol.generateRandomBoard();

		do {
			generationCount++;
			gol.computeNextGenerationBoard();
			gol.printBoardToCmdLine();

			if (System.in.available() > 0) {
				doBreak = true;
			}
			Thread.sleep(1000);
		} while (generationCount < 100 || doBreak);

	}

	public GOLCell[][] getBoard() {
		return board;
	}

	public void setBoard(GOLCell[][] board) {
		this.board = board;
	}

	public int getBoardX() {
		return boardX;
	}

	public int getBoardY() {
		return boardY;
	}

	public void generateRandomBoard() {
		GOLCell[][] newBoard = new GOLCell[boardX][boardY];
		for (int y = 0; y < boardY; y++) {
			for (int x = 0; x < boardX; x++) {
				GOLCell cell = new GOLCell();
				if (Math.random() > 0.5) {
					cell.setAlive(true);
				}
				newBoard[x][y] = cell;
			}
		}

		board = newBoard;
	}

	public void printBoardToCmdLine() {
		System.out.println("Generation:" + generationCount + "\n");
		for (int y = 0; y < boardY; y++) {
			for (int x = 0; x < boardX; x++) {
				System.out.print(board[x][y].isAlive() ? "x" : " ");
			}
			System.out.println();
		}
	}

	public void computeNextGenerationBoard() {
		GOLCell[][] newBoard = new GOLCell[boardX][boardY]; // all false
		for (int y = 0; y < boardY; y++) {
			for (int x = 0; x < boardX; x++) {
				GOLCell newCell = new GOLCell();
				newBoard[x][y] = newCell;

				boolean cellAlive = board[x][y].isAlive();
				int alive = countAliveNeighbors(x, y);
				// dead cell with three neighbors is reborn
				if (!cellAlive && alive == 3) {
					newBoard[x][y].setAlive(true);
					newBoard[x][y].setAge(board[x][y].getAge() + 1);
				} else if (cellAlive && alive < 2) {
					// alive cells with less than 2 neighbors die
				} else if (cellAlive && (alive == 2 || alive == 3)) {
					// alive cells with 2 or 3 alive neighbors stay alive
					newBoard[x][y].setAlive(true);
					newBoard[x][y].setAge(board[x][y].getAge() + 1);
				} else if (cellAlive && alive > 3) {
					// alive cells with more than 3 alive neighbors die
				}
			}
		}
		board = newBoard;

	}

	public int countAliveNeighbors(int x, int y) {
		int alive = 0;
		if (isAlive(x - 1, y)) {
			alive++;
		}
		if (isAlive(x - 1, y - 1)) {
			alive++;
		}
		if (isAlive(x, y - 1)) {
			alive++;
		}
		if (isAlive(x + 1, y - 1)) {
			alive++;
		}
		if (isAlive(x + 1, y)) {
			alive++;
		}
		if (isAlive(x + 1, y + 1)) {
			alive++;
		}
		if (isAlive(x, y + 1)) {
			alive++;
		}
		if (isAlive(x - 1, y + 1)) {
			alive++;
		}
		return alive;
	}

	private boolean isAlive(int x, int y) {
		if (x < 0 || x >= boardX || y < 0 || y >= boardY) {
			return false;
		} else {
			return board[x][y].isAlive();
		}
	}
}
