package de.buchholz.gameoflife.game;

public class GOLCell {

	private int mAge = 0;
	private boolean isAlive = false;

	public GOLCell() {

	}

	public GOLCell(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public void setAge(int age) {
		mAge = age;
	}

	public int getAge() {
		return mAge;
	}

	public int doAge() {
		return mAge++;
	}

	public void resetAge() {
		mAge = 0;
	}

	@Override
	public String toString() {
		return "GOLCell [mAge=" + mAge + ", isAlive=" + isAlive + "]";
	}

}
